const authors = [
  {
    name: 'MrEndermeshka',
    imageUrl: 'endermeshka.jpg',
    bio:
      'TL, UnityDev',
    facebook: 'https://www.facebook.com/',
    twitter: 'https://www.twitter.com/',
    instagram: 'https://www.instagram.com/',
    google: 'https://www.google.com/',
    linkedin: 'https://www.linkedin.com/',
  },
  {
    name: 'anakonda24_7',
    imageUrl: 'anakonda.jpg',
    bio:
      'Designer',
    facebook: 'https://www.facebook.com/',
    twitter: 'https://www.twitter.com/',
    instagram: 'https://www.instagram.com/',
    google: 'https://www.google.com/',
    linkedin: 'https://www.linkedin.com/',
  },
  {
    name: 'Kamin_a',
    imageUrl: 'nazarius.jpg',
    bio:
      'UnityDev',
    facebook: 'https://www.facebook.com/',
    twitter: 'https://www.twitter.com/',
    instagram: 'https://www.instagram.com/',
    google: 'https://www.google.com/',
    linkedin: 'https://www.linkedin.com/',
  },
  {
    name: 'blukenta',
    imageUrl: 'blukenta.jpg',
    bio:
      'Designer',
    facebook: 'https://www.facebook.com/',
    twitter: 'https://www.twitter.com/',
    instagram: 'https://www.instagram.com/',
    google: 'https://www.google.com/',
    linkedin: 'https://www.linkedin.com/',
  },
  {
    name: 'polonevich',
    imageUrl: 'me.jpg',
    bio:
      'Web: Firebase',
    facebook: 'https://www.facebook.com/',
    twitter: 'https://www.twitter.com/',
    instagram: 'https://www.instagram.com/',
    google: 'https://www.google.com/',
    linkedin: 'https://www.linkedin.com/',
  },
]

module.exports = authors