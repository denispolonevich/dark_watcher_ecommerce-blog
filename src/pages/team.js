import React from 'react'
import Layout from '../components/layout'
import SEO from '../components/seo'
import authors from '../util/authors'
import { Button, Card, CardText, CardBody, CardTitle, Row } from 'reactstrap'
import Au1Image from '../images/endermeshka.jpg'
import Au2Image from '../images/anakonda.jpg'
import Au3Image from '../images/nazarius.jpg'
import Au4Image from '../images/blukenta.jpg'
import Au5Image from '../images/me.jpg'
import { slugify } from '../util/utilityFunctions'

const TeamPage = () => (
  <Layout pageTitle="OUR TEAM">
    <SEO title="Our Team" keywords={[`gatsby`, `application`, `react`]} />
    <Row className="mb-4">
      <div className="col-md-3">
        <img src={Au1Image} style={{ maxWidth: '100%' }} alt="author's profile" />
      </div>
      <div className="col-md-8" >
        <Card style={{ minHeight: '100%' }}>
          <CardBody style={{backgroundColor: '#3d543b'}}>
            <CardTitle style={{color: '#1f2024'}, {fontWeight: '600'}}>{authors[0].name}</CardTitle>
            <CardText style={{color: '#1f2024'}, {fontWeight: '600'}}>{authors[0].bio}</CardText>
            <Button
              classname="text-uppercase"
              color="primary"
              href={`/author/${slugify(authors[0].name)}`}
            >
              VIEW POSTS
            </Button>
          </CardBody>
        </Card>
      </div>
    </Row>
    <Row className="mb-4">
      <div className="col-md-3">
        <img src={Au2Image} style={{ maxWidth: '100%' }} alt="author's profile" />
      </div>
      <div className="col-md-8">
        <Card style={{ minHeight: '100%' }}>
          <CardBody style={{backgroundColor: '#3d543b'}}>
            <CardTitle style={{color: '#1f2024'}, {fontWeight: '600'}}>{authors[1].name}</CardTitle>
            <CardText style={{color: '#1f2024'}, {fontWeight: '600'}}>{authors[1].bio}</CardText>
            <Button
              classname="text-uppercase"
              color="primary"
              href={`/author/${slugify(authors[1].name)}`}
            >
              VIEW POSTS
            </Button>
          </CardBody>
        </Card>
      </div>
    </Row>
    <Row className="mb-4">
      <div className="col-md-3">
        <img src={Au3Image} style={{ maxWidth: '100%' }} alt="author's profile" />
      </div>
      <div className="col-md-8">
        <Card style={{ minHeight: '100%' }}>
          <CardBody style={{backgroundColor: '#3d543b'}}>
            <CardTitle style={{color: '#1f2024'}, {fontWeight: '600'}}>{authors[2].name}</CardTitle>
            <CardText style={{color: '#1f2024'}, {fontWeight: '600'}}>{authors[2].bio}</CardText>
            <Button
              classname="text-uppercase"
              color="primary"
              href={`/author/${slugify(authors[2].name)}`}
            >
              VIEW POSTS
            </Button>
          </CardBody>
        </Card>
      </div>
    </Row>
    <Row className="mb-4">
      <div className="col-md-3">
        <img src={Au4Image} style={{ maxWidth: '100%' }} alt="author's profile" />
      </div>
      <div className="col-md-8">
        <Card style={{ minHeight: '100%' }}>
          <CardBody style={{backgroundColor: '#3d543b'}}>
            <CardTitle style={{color: '#1f2024'}, {fontWeight: '600'}}>{authors[3].name}</CardTitle>
            <CardText style={{color: '#1f2024'}, {fontWeight: '600'}}>{authors[3].bio}</CardText>
            <Button
              classname="text-uppercase"
              color="primary"
              href={`/author/${slugify(authors[3].name)}`}
            >
              VIEW POSTS
            </Button>
          </CardBody>
        </Card>
      </div>
    </Row>
    <Row className="mb-4">
      <div className="col-md-3">
        <img src={Au5Image} style={{ maxWidth: '100%' }} alt="author's profile" />
      </div>
      <div className="col-md-8">
        <Card style={{ minHeight: '100%' }}>
          <CardBody style={{backgroundColor: '#3d543b'}}>
            <CardTitle style={{color: '#1f2024'}, {fontWeight: '600'}}>{authors[4].name}</CardTitle>
            <CardText style={{color: '#1f2024'}, {fontWeight: '600'}}>{authors[4].bio}</CardText>
            <Button
              classname="text-uppercase"
              color="primary"
              href={`/author/${slugify(authors[4].name)}`}
            >
              VIEW POSTS
            </Button>
          </CardBody>
        </Card>
      </div>
    </Row>
  </Layout>
)

export default TeamPage