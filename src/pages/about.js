import React from 'react'
import Layout from '../components/layout'
import SEO from '../components/seo'

const AboutPage = () => (
  <Layout pageTitle="ABOUT US">
    <SEO title="About" keywords={[`gatsby`, `application`, `react`]} />
    <p style={{color: '#1f2024'}, {fontWeight: '600'}}>
      WE'RE JUST A SMALL INDIE COMPANY BASED IN ZHYTOMYR.
    </p>
  </Layout>
)

export default AboutPage