---
title: 'Third Post - DAY3'
date: 2022-01-12 10:00:00
author: 'Kamin_a'
image: ../../images/game.png
tags:
  - third-day
  - gamedev
---

Mastering the game engine Unity: movement, gravity.
Animation of jump, squat. The background of the first location is finished.
The first part of the necessary blocks of the site has been compiled.