---
title: 'Fifth Post - DAY5'
date: 2022-01-14 10:00:00
author: 'polonevich'
image: ../../images/game.png
tags:
  - fifth-day
  - gamedev
---

The mechanics of encountering obstacles, namely saws and spikes, have been developed. Start creating animations for the character Deer. Dialog box design developed. Mastering the database for the site.