---
title: 'Tenth Post - DAY10'
date: 2022-01-19 10:00:00
author: 'polonevich'
image: ../../images/game.png
tags:
  - tenth-day
  - gamedev
---

Logic of transition between squat and steal animations, jerk animation. Added checkpoint and interaction with it. Control of the game controller. Elaboration of the cut scene. The first room of the second location has been completed