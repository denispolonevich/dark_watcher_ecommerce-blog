---
title: 'Ninth Post - DAY9'
date: 2022-01-18 10:00:00
author: 'blukenta'
image: ../../images/game.png
tags:
  - ninth-day
  - gamedev
---

Development of settings menus, save and download files to continue the game. Deer animation at rest is created and configured. Fix texture display. Continuation of the development of the cut-scene, detailing the rooms of another location. Payment via Stripe and collections in the database, not static.