---
title: 'Seventh Post - DAY7'
date: 2022-01-16 10:00:00
author: 'anakonda24_7'
image: ../../images/game.png
tags:
  - seventh-day
  - gamedev
---

Schematic development of the 2nd location has been completed.
The development of animations for the menu is in progress.
Development of a new NPC.
Implemented animation of running character "Deer".
Home page of the site with 5 test categories, routes, test data, Stripe API connected.
Fixed software errors in the canvas, image loading, texture layers, logic of fighting with opponents, optimized the file structure of the program.