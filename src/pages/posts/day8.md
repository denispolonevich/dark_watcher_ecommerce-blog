---
title: 'Eighth Post - DAY8'
date: 2022-01-17 10:00:00
author: 'Kamin_a'
image: ../../images/game.png
tags:
  - eighth-day
  - gamedev
---

Made a layout on the gamepad, main menu + cursor. Getting started on the storage system. Emotion profiles, continuation of the black hole animation, the process of developing the second location. Start developing game icons. At the expense of the back part: creation of an entrance, collections of users who registered manually or through a Google account. A working basket was created, with the ability to dynamically change the number of products, or - delete.