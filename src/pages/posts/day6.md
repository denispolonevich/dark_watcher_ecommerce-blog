---
title: 'Sixth Post - DAY6'
date: 2022-01-15 10:00:00
author: 'MrEndermeshka'
image: ../../images/game.png
tags:
  - sixth-day
  - gamedev
---

The schematic level design of the scene of the second game location is created, the scheme of "Bosfight" is developed, preparation of visual materials for creation of game cutscenes, embodiment of the received knowledge about firebase with use of React technology