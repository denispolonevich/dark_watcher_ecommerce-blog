---
title: 'Fourth Post - DAY4'
date: 2022-01-13 10:00:00
author: 'blukenta'
image: ../../images/game.png
tags:
  - fourth-day
  - gamedev
---

Developed a system of dialog boxes, seamless background movement. Animation of sitting motion, deer character design, initial stage of menu design development. Completion of the test layout.