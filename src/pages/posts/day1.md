---
title: 'First Post - DAY1'
date: 2022-01-10 10:00:00
author: 'MrEndermeshka'
image: ../../images/game.png
tags:
  - first-day
  - gamedev
---

Held an introductory meeting, passed the initial briefing. They chose the topic of practice. "Game in the genre of 2d platformer subway 'Dark Watcher'". python programming language. Market research was conducted. Search for references for game textures. Study of game engine mechanics.