import PropTypes from 'prop-types'
import React from 'react'

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap'

class Header extends React.Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      isOpen: false,
    }
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    })
  }
  render() {
    return (
      <div>
        <Navbar light expand="lg" color='secondary'>
          <div className="container" style={{color: '#1f2024'}, {fontWeight: '500'}}>
          <NavbarBrand href="/">{this.props.siteTitle}</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
              <NavItem>
                </NavItem>
                <NavItem>
                  <NavLink href="/team">OUR TEAM</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/tags">TAGS</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/about">ABOUT US</NavLink>
                </NavItem>
              </Nav>
            </Collapse>
          </div>
        </Navbar>
      </div>
    )
  }
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header